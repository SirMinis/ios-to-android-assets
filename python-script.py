import os
import sys
import subprocess


def make_dir(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)


make_dir('drawable-ldpi')
make_dir('drawable-mdpi')
make_dir('drawable-hdpi')
make_dir('drawable-xhdpi')
make_dir('drawable-xxhdpi')

dir_path = os.getcwd()
script_path = dir_path + '/ios2android'
ios_images_catalog_path = dir_path + '/images'
tree = os.walk(ios_images_catalog_path)

for root, dirs, files in tree:
    for f in files:
     	if f.endswith(".png"):
     		filepath = os.path.join(root, f)
     		subprocess.check_call([script_path, dir_path, filepath, f])